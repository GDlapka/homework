<?php


namespace vendor\core\base;


use app\controllers\Page;

/**
 * Class View
 * @package vendor\core\base
 * Базовый класс вида
 */
class View
{
    /**
     * текущий маршрут
     * @var array
     */
    public array $route = [];

    /**
     * текущий вид
     * @var string
     */
    public string $view;

    /**
     * текущий шаблон
     * @var string
     */
    public string $layout;

    /**
     * View constructor.
     * @param $route геттер
     * @param string $view геттер
     * @param string $layout геттер
     */
    public function __construct($route, $view = '', $layout = '')
    {
        $this->route = $route;
        if($layout === false){
            $this->layout = false;
        } else {
            $this->layout = $layout ?: LAYOUT;
        }
        $this->view = $view;
    }

    public function render($userVars)
    {
        if (is_array($userVars)) {
            extract($userVars);
        }
        $fileView = APP . "/views/{$this->route['controller']}/{$this->view}.php";
        ob_start();
        if (is_file($fileView)){
            require $fileView;
        } else {
            echo "<p>Вид <b>$fileView</b> не найден</p>";
        }
        $content = ob_get_clean();

        if ($this->layout){
            $fileLayout = APP . "/views/layouts/{$this->layout}.php";
            if (is_file($fileLayout)) {
                require $fileLayout;
            } else {
                echo "<p>Шаблон <b>$fileLayout</b> не найден</p>";
            }
        }
    }


}