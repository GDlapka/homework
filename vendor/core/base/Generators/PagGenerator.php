<?php


namespace vendor\core\base\Generators;

/**
 * Class PagGenerator
 * используется для простой генерации панели пагинации
 *
 * Использование:
 * $_SESSION['pagination']  = PagGenerator::generate($page, $pagesNum);
 *
 * @package vendor\core
 */
class PagGenerator extends \vendor\core\base\HTMLGenerator
{
    /**
     * Ассоциативный массив кода.
     */
    private const PHRASE = [
        '<li class="page-item',
        'liclass' => '"><a class="page-link',
        'aclass' => '" href="/notes/',
        'param' =>'">',
        'inner' => "</a></li>\n",
    ];

    /**
     * генерация одного элемента панели пагинации
     * @param array $params - массив, если ключ совпадает с ключом массива PHRASE,
     * - то подставляется значение этого ключа
     * @return string - строка вывода кода html
     */
    protected static function generateItem(array $params = [])
    {
        $item = '';
        foreach (self::PHRASE as $key => $value){
            if (array_key_exists($key, $params)) {
                $item .= $params[$key];
            }
            $item .= $value;
        }
//        codeDebug($item);
        return $item;
    }

    /**
     * генерация одного элемента, содержащего номер
     * @param $number - номер элемента
     * @param false $isCurrent - если true - то элемент выделяется цветом, становится "текущим"
     * @return string - строка вывода кода html
     */
    protected static function generateNumberedPag($number, $isCurrent = false)
    {
        $param = '?p=' . $number;
        if ($isCurrent) {
            return self::$content .= self::generateItem(['liclass' => ' disabled', 'aclass' => ' bg-info', 'inner' => "{$number}"]);
        }
        return self::$content .= self::generateItem(['inner' => "{$number}", 'param' => $param]);
    }

    /**
     * генерация одного управляющего элемента
     * управление передается в alias Router
     * @param $inner - внешний вид элемента
     * @param $alias - передаваемый alias (next|prev|begin|end)
     * @return string - строка вывода кода html
     */
    protected static function generateControlPag($inner, $alias)
    {
        return self::$content .= self::generateItem(['inner' => "{$inner}", 'param' => "$alias"]);
    }

    /**
     * Основная функция-генератор
     * Генерирует элементы панели пагинации
     * при любом числе страниц и любом номере страницы
     * @param $page - текущий номер страницы
     * @param $pageNum - общее количество страниц
     * @param int $range - диапазон генирируемых номеров в одну сторону
     * @return string - строка вывода кода html - геттер self::$content;
     */
    public static function generate($page, $pageNum, $range = 3)
    {
        if ($pageNum == 1)
            return null;

        self::$content = '';
        $start = 1;
        $end = $pageNum;

        $start = $page - $range;
        $end = $page + $range;

        if ($start < 1)
            $start = 1;
        if ($end > $pageNum)
            $end = $pageNum;

        if ($page > 1) {
            self::generateControlPag('<<<', 'begin');
            self::generateControlPag('<', 'prev');
        }

        for ($i = $start; $i <= $end; $i++) {
            if ($i == $page)
                self::generateNumberedPag($page, true);
            else
                self::generateNumberedPag($i);
        }

        if ($page < $pageNum) {
            self::generateControlPag('>', 'next');
            self::generateControlPag('>>>', 'end');
        }

        return self::$content;
    }

}