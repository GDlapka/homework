<?php


namespace vendor\core\base\Generators;

class TableGenerator extends \vendor\core\base\HTMLGenerator
{
    private const REPLACES = [
        'notes_username' => 'Имя пользователя',
        'notes_email' => 'e-mail',
        'notes_homepage' =>'Домашняя страница',
        'notes_message' => 'Сообщение',
    ];

    public static function generate($arr, $strLimit = 600)
    {
        self::$content = "<table class=\"table\">\n<thead>\n";
        $content = '';
        $content .= "<table class=\"table notes\">\n<thead>\n<tr>\n";

        foreach ($arr[0] as $key => $value) {
            $item = $key;
            if (keyAlive($key, self::REPLACES)){
                $item = self::REPLACES[$key];
            }
            if ($key == 'notes_id')
                continue;
            $content .= "<th scope=\"col\">" . $item . "</th>\n";
        }
        $content .= "</tr>\n</thead>\n";
        $content .= "<tbody>\n";
        foreach ($arr as $item){
            $content .= "<tr";

            $notesID = '';
            $trInner = '';
            $isFirst = true;
            foreach ($item as $key => $value) {
                if ($key == 'notes_id'){
                    $notesID = $value;
                    continue;
                }
                $multidot = '';
                if (strlen($value) > $strLimit) {
                    $multidot = '...';
                }
                if ($isFirst) {
                    $trInner .= "<th scope=\"row\">" . substr($value, 0, $strLimit) . $multidot . "</th>\n";
                    $isFirst = false;
                } else {
                    $trInner .= "<td>" . pasteBR(substr($value, 0, $strLimit)) . $multidot . "</td>\n";
                }
            }
            if (!empty($notesID)){
                $content .= " id=\"note{$notesID}\"";
            }
            $content .= ">\n";
            $content .= $trInner;
            $content .= "</tr>\n";
        }
        $content .= "</tbody>\n</table>\n";
        self::$content .= "<tr>\n{$content}\n</tr>\n";
        return self::$content;
    }
}