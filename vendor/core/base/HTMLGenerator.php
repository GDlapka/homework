<?php


namespace vendor\core\base;

/**
 * Class HTMLGenerator
 * @package vendor\core\base
 * абстрактный класс генератора html кода
 */
abstract class HTMLGenerator
{
    /**
     * строка вывода кода
     * @var string
     */
    protected static $content = '';
}