<?php

namespace vendor\core\base;

/**
 * Class Controller
 * @package vendor\core\base
 * Базовый класс контроллера
 */
abstract class Controller
{
    public $route = [];
    public $view;
    public $layout;

    /**
     * Пользовательские переменные
     * @var array
     */
    private $vars = [];

    /**
     * сеттер для $this->vars
     * @param $vars
     */
    public function setVars($vars){
        $this->vars = $vars;
    }

    /**
     * геттер для $this->vars
     * @param $vars
     */
    public function getVars(){
        return $this->vars;
    }

    public function __construct(array $route){
        $this->route = $route;
        $this->view = $route['action'];
        //include APP . "/views/{$route['controller']}/$this->view" . '.php';
    }

    public function getView()
    {
        $vObj = new View($this->route, $this->view, $this->layout);
        $vObj->render($this->vars);
    }
}