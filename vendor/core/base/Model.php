<?php


namespace vendor\core\base;

use vendor\core\Db;

/**
 * Class Model
 * @package vendor\core\base
 * Базовый класс модели
 */
abstract class Model
{
    /**
     * ссылка на pdo подключение к базе данных
     * @var Db|null
     */
    protected $db;

    /**
     * текущая таблица
     * @var
     */
    protected $table;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->db = Db::getInstance();
    }

    /**
     * Массив для автоматического импорта в базу
     * по заданным ключам
     * @var array
     */
    protected $attributes = [];

    public function _getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Массив для записи ошибок работы с моделью
     * @var array
     */
    protected $errors = [];

    public function _exportErrors(){
        return $this->errors;
    }

    public function exportErrors(&$arr = NULL){
        if($arr === NULL) {
            $arr = [];
        }
        foreach ($this->errors as $key => $value ) {
            $arr['validateErrors'][$key] = $value;
        }
    }

    public function load($data)
    {
        $this->attributes = [];
        foreach ($data as $key => $value){
            //защита от XSS
            $this->attributes[$key] = unXSS($data[$key]);
        }
    }

    protected function getInsertStrings($arr = null)
    {
        if ($arr) {
            $this->attributes = $arr;
        }
        $keys = '';
        $values = '';
        $isFirst = true;
        foreach ($this->attributes as $key => $value) {
            if(!empty($value)){
                if($isFirst) {
                    $isFirst = false;
                } else {
                    $values .= ',';
                    $keys .= ',';
                }
                $keys .= '`' . $key . '`';
                $values .= '\'' . $value . '\'';
            }
        }
        trim($keys, ',');
        trim($values, ',');
        return ['keys' => $keys, 'values' => $values];
    }

    public function insert($arr = null)
    {
        $insertStrings = $this->getInsertStrings($arr);
        $sql = "INSERT INTO `{$this->table}` ({$insertStrings['keys']}) VALUES ({$insertStrings['values']});";
        return $this->query($sql);
    }

    public function preparePassword()
    {
        $_SESSION['signup'] = $this->attributes;
        $pass = $this->attributes['pass1'];
        unset($this->attributes['pass1']);
        unset($this->attributes['pass2']);
        $this->attributes['password'] = $pass;
    }

    public function hashPassword()
    {
        if(array_key_exists('password', $this->attributes)){
            $this->attributes['password'] = password_hash($this->attributes['password'], PASSWORD_BCRYPT);
        }
    }

    /**
     * запрос к базе
     * @param $sql
     * @return array|bool
     */
    public function execute($sql)
    {
        return $this->db->execute($sql);
    }

    /**
     * запрос к базе с возвращением результата
     * @param $sql
     * @return array|bool
     */
    public function query($sql)
    {
        return $this->db->query($sql);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM {$this->table}";
        return $this->db->query($sql);
    }

}