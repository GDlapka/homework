<?php


namespace vendor\core;

use http\Env\Url;

/**
 * Class Db
 * Singleton
 * Класс работы с базой данных
 * @package vendor\core
 */
class Db
{
    /**
     * экземпляр подключения к db
     * @var \PDO
     */
    protected $pdo;

    /**
     * единственный кземпляр класса
     * @var null
     */
    protected static $instance = NULL;

    /**
     * Db constructor.
     */
    protected function __construct()
    {
        $db = require ROOT . '/config/config_db.php';
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        ];
        $this->pdo = new \PDO($db['dsn'], $db['user'], $db['pass'], $options);
    }

    /**
     * инициализация подключения к db
     * @return Db|null
     */
    public static function getInstance()
    {
        if (self::$instance === NULL) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * pdo запрос к базе данных
     * @param string $sql строка запроса
     * @param bool $isFetching true если ожидаются возвращаемые данные
     * @return array|bool результат запроса
     */
    public function query(string $sql, bool $isFetching = true)
    {
        $stmp = $this->pdo->prepare($sql);
        $res = $stmp->execute();
        if (!($isFetching))
            return $res;
        if ($res !== false){
            return $stmp->fetchAll();
        }
        return [];
    }

    /**
     * pdo SQL запрос без возвращения данных
     * @param $sql
     * @return bool
     */
    public function execute($sql)
    {
        return $this->query($sql, false);
    }
}