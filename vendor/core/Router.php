<?php

namespace vendor\core;

/**
 * Class Router
 * отвечает за маршрутизацию
 */
class Router
{
    /**
     * таблица маршрутов
     * @var array
     */
    private static $routes = [];

    /**
     * текущий маршрут
     * @var array
     */
    private static $route = [];

    /**
     * функция наполнения таблицы маршрутов
     * @param string $regexp регулярное выражение маршрута
     * @param array $route маршрут
     */
    public static function add($regexp, $route = [])
    {
        self::$routes[$regexp] = $route;
    }

    /**
     * геттер поля $routes
     * @return array
     */
    public static function getRoutes()
    {
        return self::$routes;
    }

    /**
     * геттер поля $route
     * @return array
     */
    public static function getRoute()
    {
        return self::$route;
    }

    /**
     * поиск совпадений в таблице маршрутов
     * @param string $url текущий url
     * @return boolean
     */
    private static function matchRoute(string $url)
    {
        foreach (self::$routes as $pattern => $route) {
            if (preg_match("#$pattern#i", $url, $matches)){
                foreach ($matches as $key => $value){
                    if (is_string($key)){
                        $route[$key] = $value;
                    }
                }
                $route['action'] ??= 'index';
                $route['controller'] = kebabToPascalCase($route['controller']);
                self::$route = $route;
                return true;
            }
        }
        return false;
    }

    /**
     * Перенаправляет URL по конкретному маршруту
     * @param string $url входящий URL
     * @return void
     */
    public static function dispatch(string $url)
    {
        $url = self::removeQueryString($url);
        if (self::matchRoute($url)){
            $className = self::$route['controller'];
            $className = 'app\\controllers\\' . $className;
            $className =  kebabToPascalCase($className) . 'Controller';
            if (class_exists($className)) {
                $cObj = new $className(self::$route);
                $action = self::$route['action'];
                $action = kebabToCamelCase($action) . 'Action';
                if (method_exists($cObj, $action)){
                    $cObj->$action();
                    $cObj->getView();
                } else {
                    echo "<br>Метод <b>$className::$action</b> не найден";
                }
            } else {
                echo "Контроллер <b>$className</b> не найден!";
            }
        } else {
            http_response_code(404);
            include '404.html';
        }
    }

    private static function removeQueryString($url)
    {
        if ($url){
            $params = explode('&', $url, 2);
            if (false === strpos($params[0], '=')){
                return rtrim($params[0], '/');
            } else {
                return '';
            }
        }
        return $url;
    }

}