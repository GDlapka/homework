<?php

use JetBrains\PhpStorm\NoReturn;

/**
 * Паттерн для проверки соответствия url
 */
$urlPattern = '/(?:([^\:]*)\:\/\/)?(?:([^\:\@]*)(?:\:([^\@]*))?\@)?(?:([^\/\:]*)\.(?=[^\.\/\:]*\.[^\.\/\:]*))?([^\.\/\:]*)(?:\.([^\/\.\:]*))?(?:\:([0-9]*))?(\/[^\?#]*(?=.*?\/)\/)?([^\?#]*)?(?:\?([^#]*))?(?:#(.*))?/';

/**
 * вывод массива в удобном виде для отладки
 * @param $arr
 */
function debug($arr)
{
    echo '<pre>' . print_r($arr, true) . '</pre>';
}

/**
 * вывод массива в удобном виде для отладки с распечаткой кода html
 * @param $arr
 */
function codeDebug($arr)
{
    echo '<pre>' . htmlentities(print_r($arr, true)) . '</pre>';

}

/**
 * Функция защиты вводимых данных от XSS-атак
 * @param string $str вводимоя строка
 * @return string  строка на выходе, кодированная от тегов
 */
function unXSS(string $str):string
{
    return htmlspecialchars(strip_tags($str), ENT_QUOTES, "UTF-8");
}

/**
 * Преобразует строку нотации kebab в строку нотации Pascal
 * @param string $str в kebabCase
 * @return string в PascalCase
 */
function kebabToPascalCase(string $str): string
{
    $words = explode('-', $str);
    $result = '';
    foreach ($words as $word) {
        $result .= ucwords($word);
    }
    return $result;
}

/**
 * Функция преобразования строки "Шашлычной нотации"
 * в строку "Верблюжей нотации"
 * @param string $str
 * @return string
 */
function kebabToCamelCase(string $str): string
{
    $result = kebabToPascalCase($str);
    return lcfirst($result);
}

/**
 * Функция редиректа
 * @param bool $http
 */
function redirect($http = false)
{
    $redirect = $http ?: ($_SERVER['HTTP_REFERER'] ?? '/');
//    $redirect = $http ?: (isset($_SERVER['HTTP_REFERER']) ?: '/');
    header("Location: $redirect");
    exit();
}

/**
 * Функция распечатки массива строк
 * в HTML формате
 * * @return string возвращает html список
 */
function arrayToList($arr)
{
    $list = '';
    foreach ($arr as $item) {
        echo '<li>' . $item . '</li>';
    }
    return $list;
}

/**
 * Функция проверки массива на пустые ключи / несуществующие значения
 * заданных ключей / наличие пробелов
 * Ключи задаются в массиве $parameters
 * @param array $arr искомый массив
 * @param array $parameters массив проверяемых ключей
 * @return array массив с пустыми значениями/несуществующими ключами
 */
function hasEmptyOrUnsetValues(array $arr, array $parameters) :array
{
    $result = [];
    foreach ($parameters as $param){
        if (!array_key_exists($param, $arr)){
            $result[] = $param;
        } elseif (empty($arr[$param])){
            $result[] = $param;
        }
    }
    return $result;
}

/**
 * Детектор пробелов
 * @param $arr массив строк
 * @return array массив ключей с обнаруженными пробелами
 */
function detectSpaces($arr){
    $result = [];
    foreach ($arr as $key => $value) {
        if (preg_match('#[\s]+#', $value)) {
            $result[] = $key;
        }
    }
    return $result;
}

/**
 * Функция проверяет существование и неравность нулю
 * для произвольного ключа массива
 * @param $key Проверяемый ключ массива
 * @param array $arr Проверяемый массив
 * @return bool
 */
function keyAlive($key, array $arr)
{
    return array_key_exists($key, $arr) && !empty($arr[$key]);
}

/**
 * Функция замены переносов строк на тег <br>
 * @param string $text
 * @return string|string[]
 */
function pasteBR($text)
{
    return str_replace(array("\r\n", "\r", "\n"), '<br>', $text);
}