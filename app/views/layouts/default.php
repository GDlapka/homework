<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <!-- user CSS -->
    <link href="/public/css/main.css" rel="stylesheet">

    <title>Гостевая книга</title>
    <script src="/public/js/functions.js" type="text/javascript"></script>
</head>
<body>

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>

<?php //debug($_SESSION); ?>
<?php //$_SESSION = []; ?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand">Гостевая:</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Домой</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/add">Новое сообщение</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Сортировать по:
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="/notes/timedown">Сначала старые</a></li>
                        <li><a class="dropdown-item" href="/notes/timeup">Сначала новые</a></li>
                        <li><a class="dropdown-item" href="/notes/namedown">Имя a>z а>я</a></li>
                        <li><a class="dropdown-item" href="/notes/nameup">Имя z>a я>а</a></li>
                        <li><a class="dropdown-item" href="/notes/emaildown">Email a>z а>я</a></li>
                        <li><a class="dropdown-item" href="/notes/emailup">Email z>a я>а</a></li>
                    </ul>
                </li>
                <form class="d-flex" action="/notes/search" method="get">
                    <input class="form-control" type="search" placeholder="Поиск" aria-label="Search" name="q" <?= (keyAlive('search', $_SESSION) && (!array_key_exists('stop', $_GET))) ? "value=\"{$_SESSION['search']}\"" : ''?>>
                    <input type="submit" value="X" title="Отменить поиск" name="stop">
                    <button class="btn btn-outline-success" type="submit">Найти</button>
                </form>
            </ul>

            <form class="d-flex">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" href="<?=keyAlive('login', $_SESSION) ? "/notes/edit" : "/user/signup"?>" aria-current="page"> <?=keyAlive('login', $_SESSION) ? "Редактор сообщений" : "Регистрация"?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="<?=keyAlive('login', $_SESSION) ? '/user/logout' : '/user/signin'?>">
                            <?=array_key_exists('login', $_SESSION) ? '(' . $_SESSION['login'] . ') Выход' : 'Войти'?>
                        </a>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</nav>

<?=$content?>

</body>
</html>
