<script type="text/javascript">

    function setValidatorView(){
        const errors = new Map();
        <?php if(array_key_exists('validateErrors', $_SESSION)): ?>
            <?php $arr = $_SESSION['validateErrors']; ?>
            <?php foreach ($arr as $key => $value): ?>
        errors.set("<?=$key?>","<?=$value?>");
            <?php endforeach; ?>
        <?php endif; ?>

        const signup = new Map();
        <?php if(array_key_exists('signup', $_SESSION)): ?>
            <?php $arr = $_SESSION['signup']; ?>
            <?php foreach ($arr as $key => $value): ?>
                <?php if(!empty($value)): ?>
        signup.set("<?=$key?>","<?=$value?>");
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>

        setValidateClasses(errors);
        setFiels(signup);
    }

    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        let forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })()
</script>
<div class="container-lg bg-light input-box">
    <form class="row g-2" action="/user/signup" method="post">
        <div class="col-md-4">
            <label for="validationServerUsername" class="form-label">Имя пользователя</label>
            <div class="input-group has-validation">
                <span class="input-group-text" id="inputGroupPrepend3">@</span>
                <input type="text" class="form-control" id="validationServerUsername"
                       aria-describedby="inputGroupPrepend3 validationServerUsernameFeedback" name="username" autofocus required>
                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                    Пользователь уже существует!
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <label for="validationServerPass1" class="form-label">Пароль</label>
            <div class="input-group has-validation">
                <input type="password" class="form-control" id="validationServerPass1"
                       aria-describedby="inputGroupPrepend3 validationServerPass1Feedback" name="pass1" required>
                <div id="validationServerPass1Feedback" class="invalid-feedback">
                    Пароли не совпадают.
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <label for="validationServerPass2" class="form-label">Пароль повторно</label>
            <div class="input-group has-validation">
                <input type="password" class="form-control" id="validationServerPass2"
                       aria-describedby="inputGroupPrepend3 validationServerPass2Feedback" name="pass2" required>
                <div id="validationServerPass2Feedback" class="invalid-feedback">
                    Пароли не совпадают.
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <label for="validationServerEmail" class="form-label">E-mail</label>
            <input type="text" class="form-control" id="validationServerEmail"
                   aria-describedby="validationServerEmailFeedback" name="email" required>
            <div id="validationServerEmailFeedback" class="invalid-feedback">
                e-mail некорректен.
            </div>
        </div>
        <div class="col-md-4">
            <label for="validationServerHomepage" class="form-label">Домашняя страница:</label>
            <input type="text" class="form-control" name="homepage" id="validationServerHomepage">
            <div id="validationServerHomepageFeedback" class="invalid-feedback">
                URL указан неверно.
            </div>
        </div>
        <div class="col-md-4">
            <button class="btn btn-dark" id="but-reg-submit" type="submit">Зарегистрироваться</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    setValidatorView();
</script>