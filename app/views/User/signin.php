<script type="text/javascript">

    function setValidatorView(){
        const errors = new Map();
        <?php if(array_key_exists('validateErrors', $_SESSION)): ?>
        <?php $arr = $_SESSION['validateErrors']; ?>
        <?php foreach ($arr as $key => $value): ?>
        errors.set("<?=$key?>","<?=$value?>");
        <?php endforeach; ?>
        <?php endif; ?>
        setValidateClasses(errors);

        const signup = new Map();
        <?php if(array_key_exists('signup', $_SESSION)): ?>
        <?php $arr = $_SESSION['signup']; ?>
        <?php foreach ($arr as $key => $value): ?>
        <?php if(!empty($value)): ?>
        signup.set("<?=$key?>","<?=$value?>");
        <?php endif; ?>
        <?php endforeach; ?>
        <?php endif; ?>
        setFiels(signup)
    }

    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        let forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })()
</script>
<div class="container-lg bg-light input-box" id="signin-box">
    <legend align="center">Авторизируйтесь, чтобы редактировать Ваши сообщения.</legend>
    <br>
    <form action="/user/signin" method="post">
        <div class="container-flex col align-self-start">
            <label for="validationServerUsername" class="form-label">Имя пользователя (admin|admin)</label>
            <div class="input-group has-validation">
                <span class="input-group-text" id="inputGroupPrepend3">@</span>
                <input type="text" class="form-control" id="validationServerUsername"
                       aria-describedby="inputGroupPrepend3 validationServerUsernameFeedback" name="username" autofocus required>
                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                    Пользователь не зарегистрирован!
                </div>
            </div>
        </div>
        <div class="container-flex">
            <label for="validationServerPassword" class="form-label">Пароль</label>
            <div class="input-group has-validation">
                <input type="password" class="form-control" id="validationServerPass1"
                       aria-describedby="inputGroupPrepend3 validationServerPasswordFeedback" name="password" required>
                <div id="validationServerPasswordFeedback" class="invalid-feedback">
                    Неверный пароль!
                </div>
            </div>
        </div>
        <div class="container">
            <div class="container-sm-2 al-center">
                <button type="submit" class="btn btn-primary">Войти</button>
            </div>
        </div>

    </form>
</div>
<script type="text/javascript">
    setValidatorView();
</script>