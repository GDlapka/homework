<script type="application/javascript">
    <?php if(array_key_exists('message', $_SESSION)): ?>
        <?php if(!empty($_SESSION['message'])): ?>
    alert('<?=$_SESSION['message']; ?>');
            <?php unset($_SESSION['message']); ?>
        <?php endif; ?>
    <?php endif; ?>

    $(document).ready(function () {
        $(document).click(function () {
            if ($('div.messageBox').css('display') == 'block') {
                 $('div.messageBox').hide();
            }
        });
        $('div.messageBox').click(function (){
            if ($('div.messageBox').css('display') == 'block') {
                $('div.messageBox').hide();
            }
        });

        $("table.notes > tbody").delegate('tr', 'click', function() {
            isMessage = true;
            let noteID = $(this).attr('id').replace(/[^0-9]/g,"");
            $.ajax({
                method: "POST",
                url:    "getmessage",
                data:   {id: noteID}
            })
            .done(function (msg) {
                let message = msg.toString().replace(/(?:\r\n|\r|\n)/g, '<br />');
                $("div.messageBox").html(message);
                $("div.messageBox").css('display', 'block');
            });
        });
    });

</script>

<form method="POST">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3"><label class="form-label">Сообщений на странице:</label></div>
                        <div class="col-sm-2"><input type="text" class="form-control"
                             name="pOffset" id="pOffset" aria-describedby="emailHelp"
                             value="<?= (keyAlive('pOffset',$_SESSION) ? $_SESSION['pOffset'] : pOffset) ?>"></div>
                    </div>
                </div>
            </div>
            <div class="col"></div>
            <div class="col"></div>
        </div>
    </div>
</form>


<?=$_SESSION['table']?>
<div class="container-fluid">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
<?= (array_key_exists('pagination', $_SESSION) ? $_SESSION['pagination'] : '') ?>
        </ul>
    </nav>
</div>

<div class="container messageBox">Once <br> Twice</div>