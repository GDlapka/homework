<script type="text/javascript">

    function setValidatorView(){
        const errors = new Map();
        <?php if(array_key_exists('validateErrors', $_SESSION)): ?>
        <?php $arr = $_SESSION['validateErrors']; ?>
        <?php foreach ($arr as $key => $value): ?>
        errors.set("<?=$key?>","<?=$value?>");
        <?php endforeach; ?>
        <?php endif; ?>

        const post = new Map();
        <?php if(array_key_exists('post', $_SESSION)): ?>
        <?php $arr = $_SESSION['post']; ?>
        <?php foreach ($arr as $key => $value): ?>
        <?php if(!empty($value)): ?>
        post.set("<?=$key?>","<?=$value?>");
        <?php endif; ?>
        <?php endforeach; ?>
        <?php endif; ?>

        setValidateClasses(errors);
        setFiels(post);
    }

    $(document).ready(function (){
        // заполняем переменную данными, при изменении значения поля file
        $('#upload').change(function(){
            console.log('start');
            if (window.FormData !== undefined) {
                let words = $("#upload")[0].value.split(".");
                let ext = words[words.length - 1];

                let formData = new FormData();
                formData.append('file', $("#upload")[0].files[0]);
                formData.append('ext',ext);

                $.ajax({
                    type: "POST",
                    url: '/upload',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: formData,
                    dataType : 'json',
                    success: function (msg) {
                        if (msg.error == '') {
                            $("#upload").hide();
                            $('div.ajax-reply').html(msg.success);
                            $('#filename').val(msg.tempName);
                        } else {
                            $('div.ajax-reply').html(msg.error);
                        }
                    }
                });
            }
        });
    });


    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        let forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })()
</script>
<div class="container-lg bg-light input-box">
    <form class="row g-2" action="/add" method="post">
        <div class="col-md-4">
            <label for="validationServerUsername" class="form-label">Имя</label>
            <div class="input-group has-validation">
                <span class="input-group-text" id="inputGroupPrepend3">@</span>
                <input
                        type="text"
                        class="form-control"
                        id="validationServerUsername"
                        aria-describedby="inputGroupPrepend3 validationServerUsernameFeedback"
                        name="username"
                        required
                        <?= keyAlive('login', $_SESSION) ? "value=\"{$_SESSION['login']}\"" : 'autofocus' ?>
                >
                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                    Неверное имя пользователя.
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <label for="validationServerEmail" class="form-label">E-mail</label>
            <input
                    type="text"
                    class="form-control"
                    id="validationServerEmail"
                    aria-describedby="validationServerEmailFeedback"
                    name="email"
                    required
                <?= keyAlive('email', $_SESSION) ? "value=\"{$_SESSION['email']}\"" : '' ?>
            >
            <div id="validationServerEmailFeedback" class="invalid-feedback">
                e-mail некорректен.
            </div>
        </div>
        <div class="col-md-4">
            <label for="validationServerHomepage" class="form-label">Домашняя страница:</label>
            <input
                    type="text"
                    class="form-control"
                    name="homepage"
                    id="validationServerHomepage"
                    <?= keyAlive('homepage', $_SESSION) ? "value=\"{$_SESSION['homepage']}\"" : '' ?>
            >
            <div id="validationServerHomepageFeedback" class="invalid-feedback">
                URL указан неверно.
            </div>
        </div>
        <div class="input-group">
            <span class="input-group-text">Текст сообщения</span>
            <textarea
                    class="form-control"
                    aria-label="With textarea"
                    rows="15"
                    name="message"
                    required
                    <?= keyAlive('login', $_SESSION) ? 'autofocus' : '' ?>>
            </textarea>
        </div>

        <input type="file" multiple="multiple" accept=".txt,image/*" id="upload">
        <div class="ajax-reply"></div>
        <input type="text" name="file" id="filename" value="1.1" hidden>

        <div class="container-fluid">
            <button class="btn btn-dark" type="submit">Отправить</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    setValidatorView();
</script>
