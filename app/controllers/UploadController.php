<?php

namespace app\controllers;

/**
 * Class UploadController
 * @package app\controllers
 * Контроллер прикрепления файлов к сообщению
 */
class UploadController extends AppController
{
    /**
     * Основной action
     */
    public function indexAction()
    {
        // Название <input type="file">
        $input_name = 'file';

        // Разрешенные расширения файлов.
        $allow = array(
            'txt', 'jpg', 'gif', 'png', 'tmp',
        );

        // Запрещенные расширения файлов.
        $deny = array(
            'phtml', 'php', 'php3', 'php4', 'php5', 'php6', 'php7', 'phps', 'cgi', 'pl', 'asp',
            'aspx', 'shtml', 'shtm', 'htaccess', 'htpasswd', 'ini', 'log', 'sh', 'js', 'html',
            'htm', 'css', 'sql', 'spl', 'scgi', 'fcgi', 'exe'
        );

        // Директория куда будут загружаться файлы.
        $path = UPLOADS_TEMP;

        $error = $success = '';
        if (!isset($_FILES[$input_name])) {
            $error = 'Файл не загружен.';
        } else {
            $file = $_FILES[$input_name];

            // Проверим на ошибки загрузки.
            if (!empty($file['error']) || empty($file['tmp_name'])) {
                $error = 'Не удалось загрузить файл.';
            } elseif ($file['tmp_name'] == 'none' || !is_uploaded_file($file['tmp_name'])) {
                $error = 'Не удалось загрузить файл.';
            } else {
                $name = $file['tmp_name'];
                $parts = pathinfo($name);
                if (empty($name) || empty($parts['extension'])) {
                    $error = 'Недопустимый тип файла';
                } elseif (!empty($allow) && !in_array(strtolower($parts['extension']), $allow)) {
                    $error = 'Недопустимый тип файла';
                } elseif (!empty($deny) && in_array(strtolower($parts['extension']), $deny)) {
                    $error = 'Недопустимый тип файла';
                } else {
                    //считываем контрольную сумму и переименовываем файл соответственно
                    $tempName = hash_file('sha256', $file['tmp_name']);
                    $tempName .= '.' . ((keyAlive('ext', $_POST)) ? $_POST['ext'] : $parts['extension']);

                    // Перемещаем файл в директорию.
                    if (move_uploaded_file($file['tmp_name'], $path . '/' . $tempName)) {
                        // Далее можно сохранить название файла в БД и т.п.
                        $success = 'Файл «' . $name . '» успешно загружен.';
                    } else {
                        $error = 'Не удалось загрузить файл.';
                    }
                }
            }
        }

        $data = array(
            'error' => $error,
            'success' => $success,
            'tempName' => $tempName, //возвращаем новое имя файла
        );

        header('Content-Type: application/json');
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit();
    }
}
