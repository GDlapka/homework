<?php


namespace app\controllers;


use app\models\User;

/**
 * Class UserController
 * @package app\controllers
 * Контроллер действий с учетными записями пользователей
 */
class UserController extends AppController
{
    /**
     * Action регистрации пользователей
     */
    public function signupAction()
    {
        if(!empty($_POST)) {
            $model = new User();
            $model->load($_POST);
            $res = $model->validate();
            $output = null;
            if ($res){
                $model->hashPassword();
                $output = $model->insert();
            }
            $model->exportErrors($_SESSION);

            if ($res) {
                unset($_SESSION['signup']);
                //redirect
                if(!empty($output)){
                    $_SESSION['message'] = 'Возникла ошибка. Попробуйте позже.';
                } else {
                    $_SESSION['message'] = 'Вы успешно зарегистрированы!';
                }
                redirect('/');
            }
        } else {
            unset($_SESSION['signup']);
            unset($_SESSION['validateErrors']);
        }

    }

    /**
     * Action авторизации пользователей
     */
    public function signinAction()
    {
        if (keyAlive('login', $_SESSION)){
            unset($_SESSION['signup']);
            //redirect
            redirect('/');
        }
        if(!empty($_POST)) {
            $model = new User();
            $model->load($_POST);
            $res = $model->checkLogPass();
            $model->exportErrors($_SESSION);

            if ($res){
                unset($_SESSION['signup']);
                //redirect
                redirect('/');
            }
        } else {
            unset($_SESSION['validateErrors']);
            unset($_SESSION['signup']);
        }

    }

    /**
     * Action выхода
     */
    public function logoutAction()
    {
        $arr = [];
        if (keyAlive('pOffset', $_SESSION)){
            $arr['pOffset'] = $_SESSION['pOffset'];
        }
        if (keyAlive('sort', $_SESSION)){
            $arr['sort'] = $_SESSION['sort'];
        }
        $_SESSION = $arr;

        redirect('/');
    }
}