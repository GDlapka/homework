<?php
namespace app\controllers;

use app\models\Main;
use app\models\Post;
use \vendor\core\base\Generators\PagGenerator;
use \vendor\core\base\Generators\TableGenerator;

/**
 * Class BookController
 * @package app\controllers
 * Основной контроллер отображения записей
 */
class BookController extends AppController
{
    /**
     * @var int
     * Количество отображаемых записей на странице
     */
    protected $pOffset = 25;

    /**
     * Функция подготавливает запрос по алиасу на количество записей
     * для рассчета пагинации
     */
    protected function getCount()
    {

    }

    protected $model;

    public function __construct($route)
    {
        parent::__construct($route);
        $this->model = new Main();
    }

    protected function getContent($noteStart = null, $noteEnd = null)
    {
        $sql = "SELECT SQL_NO_CACHE a.`notes_id`, a.`notes_username`, a.`notes_email`,";
        $sql .= " a.`notes_homepage`, a.`notes_message` FROM (";
        $sql .= "SELECT @i:=@i+1 as num, `notes_id`, `notes_username`, `notes_email`, ";
        $sql .= "`notes_homepage`, `notes_message` FROM `notes`, (SELECT @i:=0) x";
        if (keyAlive('search', $_SESSION)) {
            $sql .= " WHERE `notes_message` LIKE '%{$_SESSION['search']}%'";
        } elseif (keyAlive('edit', $_SESSION)) {
            unset($_SESSION['edit']);
            $sql .= " WHERE `notes_username` = 'admin'";
        }
        if (keyAlive('sort', $_SESSION)) {
            $order = ' ORDER BY ';
            switch ($_SESSION['sort']) {
                case 'timeup':
                    $order .= '`notes_id` DESC';
                    break;
                case 'namedown':
                    $order .= '`notes_username`, `notes_id`';
                    break;
                case 'nameup':
                    $order .= '`notes_username` DESC, `notes_id`';
                    break;
                case 'emaildown':
                    $order .= '`notes_email`, `notes_id`';
                    break;
                case 'emailup':
                    $order .= '`notes_email` DESC, `notes_id`';
                    break;
                default:
                    $order = '';
                    break;
            }
            $sql .= $order;
        }
        $sql .= ') a';
        if ($noteStart !== null && $noteEnd !== null) {
            $sql .= " WHERE a.num >= {$noteStart} AND a.num <= {$noteEnd};";
        }
        return $this->model->query($sql);
    }

    protected function preparePagination($page, $pagesNum)
    {
        $content = PagGenerator::generate($page, $pagesNum);
        $_SESSION['pagination'] = $content;
    }

    public function notesAction()
    {
        if (keyAlive('alias', $this->route) && $this->route['alias'] == 'search') {
            if (array_key_exists('stop', $_GET)) {
                unset($_SESSION['search']);
                redirect('/');
            }
            if (array_key_exists('q', $_GET)) {
                if (empty($_GET['q'])){
                    unset($_SESSION['search']);
                    redirect('/');
                } else {
                    $_SESSION['search'] = $_GET['q'];
                    $_SESSION['page'] = 1;
                }
            }
        }
        if (keyAlive('alias', $this->route) && $this->route['alias'] == 'edit') {
            $_SESSION['edit'] = '1';
        }
        if (keyAlive('pOffset', $_POST)) {
            $this->pOffset = $_POST['pOffset'];
            $_SESSION['pOffset'] = $_POST['pOffset'];
        } elseif (keyAlive('pOffset', $_SESSION)) {
            $this->pOffset = $_SESSION['pOffset'];
        }

        $page = 1;
        if (
                keyAlive('page', $_SESSION)
            &&  !keyAlive('search', $_SESSION)
        ) {
            $page = $_SESSION['page'];
        }

        $isFilter = keyAlive('search', $_SESSION);
        if ($isFilter) {
            $sql = "SELECT SQL_NO_CACHE MAX(num) FROM ";
            $sql .= "(SELECT @i:=@i+1 as num, `notes_id`, `notes_username`, `notes_email`,";
            $sql .= " `notes_homepage`, `notes_message` FROM `notes`, (SELECT @i:=0) x";
            $sql .= " WHERE `notes_message` LIKE '%{$_SESSION['search']}%') a;";
        } else {
            $sql = "SELECT COUNT(*) FROM `notes`;";
        }
        $anwser = $this->model->query($sql);
        if (empty($anwser) && !$isFilter) {
            echo "Ошибка соединения с базой данных.";
            die();
        }

        $size = $anwser[0][$isFilter ? 'MAX(num)' : 'COUNT(*)'];
        $pagesNum = ceil($size / $this->pOffset);

        if (keyAlive('alias', $this->route)) {
            $alias = $this->route['alias'];
            switch ($alias) {
                case 'next':
                    $page++;
                    break;
                case 'prev':
                    $page--;
                    break;
                case 'end':
                    $page = $pagesNum;
                    break;
                case 'begin':
                    $page = 1;
                    break;
                case 'edit':
                    break;
                case 'search':
                    break;
                case 'getmessage':
                    if (keyAlive('id', $_POST)) {
                        $this->model->getMessage($_POST['id']);
                    }
                    break;
                default:
                    $_SESSION['sort'] = $alias;
                    if ($alias != 'timedown') {
                        $page = 1;
                    }
                    break;
            }
        } elseif (keyAlive('p', $_GET)) {
            $page = 1 * $_GET['p'];
            if ($page > $pagesNum) {
                $page = $pagesNum;
            }
            if ($page < 1) {
                $page = 1;
            }
        }

        $_SESSION['page'] = $page;

        $this->preparePagination($page, $pagesNum);

        $noteStart = $this->pOffset * ($page - 1) + 1;
        $noteEnd = $noteStart + $this->pOffset - 1;
        if ($noteEnd > $size) {
            $noteEnd = $size;
        }

        $anwser = $this->getContent();

        if (empty($anwser)) {
            if (!$isFilter) {
                echo "Ошибка соединения с базой данных. !";
                die();
            }
            $_SESSION['table'] = '<p>По данному запросу, к сожалению, ничего не найдено...</p>';
        } else {
            $_SESSION['table'] = TableGenerator::generate($anwser);
        }
    }

    public function addAction()
    {
        if (!empty($_POST)) {

            foreach ($_POST as $key => $value) {
                $_SESSION['post'][$key] = unXSS($value);
            }

            //проверка загруженного файла
            if (keyAlive('file', $_POST)) {
                $file = $_POST['file'];
                $path = UPLOADS_TEMP . '/' . $file;
                $newPath = UPLOADS . '/' . $file;
                if (file_exists($path)){
                    rename($path, $newPath);
                }
            }

            $model = new Post();
            $model->load($_POST);


            $res = $model->validate();
            if ($res) {
                $fields = $_SESSION['post'];
                $fields['notes_ip'] = $_SERVER['REMOTE_ADDR'];
                $fields['notes_browser'] = $_SERVER['HTTP_USER_AGENT'];
                $fields['notes_username'] = $_SESSION['post']['username'];
                $fields['notes_email'] = $_SESSION['post']['email'];
                $fields['notes_homepage'] = $_SESSION['post']['homepage'];
                $fields['notes_message'] = $_SESSION['post']['message'];
                $fields['notes_file'] = $_SESSION['post']['file'];
                unset($fields['username'], $fields['email'], $fields['homepage'], $fields['message'], $fields['file']);
                $model->insert($fields);
            }
            $model->exportErrors($_SESSION);
            if ($res) {
                unset($_SESSION['post']);
                //redirect
                if (!empty($output)) {
                    $_SESSION['message'] = 'Возникла ошибка. Попробуйте позже.';
                }
                redirect('/');
            }
        } else {
            unset($_SESSION['post']);
            unset($_SESSION['validateErrors']);
        }
    }
}

