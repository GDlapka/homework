<?php


namespace app\models;


/**
 * Class Post
 * @package app\models
 * Модель отправки нового сообщения
 */
class Post extends User
{
    /**
     * @return bool
     * Метод валидации введенных данных
     */
    public function validate(){
        $result = true;
        unset($_SESSION['validateErrors']);
        if(array_key_exists('post',$_SESSION)){
            $fields = $_SESSION['post'];

            //массив элементов, обязательных к заполнению:
            $params = ['username', 'email', 'message'];
            //проверка ввода обязательных полей на стороне сервера
            $broken = hasEmptyOrUnsetValues($fields, $params);
            if ($broken){
                //валидация не пройдена
                foreach ($broken as $field){
                    $this->errors[$field] = "Введите значение";
                }
                $result = false;
            }

            //проверка уникальности username
            $usernameLowCase = mb_strtolower($fields['username']);
            if (!keyAlive('login', $_SESSION)) {

                $sql = "SELECT username 
                        FROM {$this->table} 
                        WHERE username = {$usernameLowCase};";

                $anwser = $this->query($sql);

                if(!empty($anwser)) {
                    $this->errors['username'] = "Имя пользователя '" . $fields['username'] . "' уже зарегистрировано! ";
                    $this->errors['username'] .= "Вы не можете отправить сообщение от лица зарегистрированного пользователя! ";
                    $this->errors['username'] .= "Введите другое имя!";
                    $result = false;
                }
            } elseif ($usernameLowCase != $_SESSION['login']) {
                $this->errors['username'] = "Выйдите из учетной записи '"
                    . $_SESSION['login']
                    . "', чтобы добавлять сообщения от другого имени.";
                $result = false;
            }

            //проверка соответствия email
            if (keyAlive('email', $fields) && !preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i', $fields['email'])) {
                $this->errors['email'] = 'Указанный email некорректен.';
                $result = false;
            }

            //проверка правильности написания url
            global $urlPattern;
            if (!empty($fields['homepage']) && !preg_match(
                    $urlPattern,
                    $fields['homepage'])
            ) {
                $this->errors['homepage'] = 'URL указан неверно. Необязательное поле к заполнению.';
                $result = false;
            }

            //валидация пройдена
            return $result;
        }
        return false;
    }

    /**
     * @param null $arr
     * @return array|bool|void
     * Метод сохранения данных в базе
     */
    public function insert($arr = null){
        $this->table = 'notes';
        parent::insert($arr);
        $this->table = 'users';
    }
}
