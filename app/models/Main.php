<?php


namespace app\models;

use vendor\core\base\Model;

/**
 * Class Main
 * @package app\models
 * Основная модель
 * Получает сообщения из базы
 */
class Main extends Model
{
    public $table = 'notes';

    /**
     * Метод получает из базы данные по
     * @param $id
     */
    public function getMessage($id)
    {
        $sql = "SELECT notes_message, notes_username, notes_email, notes_file
                FROM notes 
                WHERE notes_id = {$id};";
        $anwser = $this->query($sql);
        if (!empty($anwser)){
            if (keyAlive('notes_username', $anwser[0])){
                echo "Пользователь: <b>{$anwser[0]['notes_username']}</b>";
                echo '<hr>';
            }
            if (keyAlive('notes_message', $anwser[0])){
                echo $anwser[0]['notes_message'];
            }
            if (keyAlive('notes_file', $anwser[0])){
                $file = $anwser[0]['notes_file'];
                $relPath = UPLOADS_RELATIVE . '/' . $file;
                $file = UPLOADS . '/' . $file;

                echo '<hr>';
                $parts = pathinfo($file);
                if (file_exists($file)){
                    switch ($parts['extension']){
                        case 'txt':
                            echo "<b>Прикреплен текстовый файл:</b><br>";
                            echo file_get_contents($file);
                            break;
                        default:
                            echo "<b>Прикреплено изображение:</b><br>";
                            echo "<img src='{$relPath}' />";
                            break;
                    }
                } else {
                    echo "<b>Прикрепленный файл не найден.</b><br>";
                }
            }
        } else {
            echo 'Ошибка. Повторите позже.';
        }
        die();
    }
}
