<?php


namespace app\models;

use vendor\core\base\Model;

/**
 * Class User
 * @package app\models
 * Модель взаимолействия с таблицей users
 */
class User extends Model
{
    public $table = 'users';

    /**
     * @return bool
     * Аутентификация
     */
    public function checkLogPass()
    {
        unset($_SESSION['validateErrors']);
        $this->attributes['username'] = mb_strtolower($this->attributes['username']);
        $_SESSION['signup'] = $this->attributes;

        $sql = "SELECT password, email, homepage
                FROM {$this->table} 
                WHERE username = '{$this->attributes['username']}';";

        $anwser = $this->query($sql);
        if($anwser === []){
            $this->errors['username'] = 'Пользователь не зарегистрирован!';
            return false;
        }
        $passwordHash = $anwser[0]['password'];
        if (!password_verify($this->attributes['password'], $passwordHash)){
            $this->errors['password'] = 'Неверный пароль!';
            return false;
        }
        $_SESSION['login'] = $this->attributes['username'];
        $_SESSION['email'] = $anwser[0]['email'];
        $_SESSION['homepage'] = $anwser[0]['homepage'];
        return true;
    }

    /**
     * @var string[]
     * Массив данных при регистрации
     */
    public $attributes = [
        'username' => '',
        'pass1' => '',
        'pass2' => '',
        'email' => '',
        'homepage' => ''
    ];

    /**
     * @return
     * Метод валидации введенных данных
     */
    public function validate()
    {
        $result = true;
        $this->preparePassword();
        unset($_SESSION['validateErrors']);
        if(array_key_exists('signup',$_SESSION)){
            $fields = $_SESSION['signup'];

            //массив элементов, обязательных к заполнению:
            $params = ['username', 'pass1', 'pass2', 'email'];
            //проверка ввода обязательных полей на стороне сервера
            $broken = hasEmptyOrUnsetValues($fields, $params);
            if ($broken){
                //валидация не пройдена
                foreach ($broken as $field){
                    $this->errors[$field] = "Введите значение";
                }
                $result = false;
            }

            //детектор пробелов
            $spaces = detectSpaces($fields);
            if ($spaces){
                foreach ($spaces as $field){
                    $this->errors[$field] = 'Обнаружены пробельные символы';
                }
                $result = false;
            }

            //проверка разрешенных символов username
            if (!preg_match('/^[a-zA-Z][a-zA-Z0-9-_\.]+$/', $fields['username'])) {
                $this->errors['username'] = 'Разрешена латиница, символы: "-" "_" "." без пробелов.';
                $result = false;
            }

            //проверка длины username
            if (mb_strlen($fields['username']) < 3 || mb_strlen($fields['username']) > 20){
                $this->errors['username'] = 'Минимум 3, максимум 20 символов в имени.';
                $result = false;
            }

            //проверка уникальности username
            $usernameLowCase = mb_strtolower($fields['username']);

            $sql = "SELECT username 
                    FROM {$this->table} 
                    WHERE username = '{$usernameLowCase}';";

            $anwser = $this->query($sql);

            if(!empty($anwser)) {
                $this->errors['username'] = "Имя пользователя '" . $fields['username'] . "' уже зарегистрировано!";
                $result = false;
            }


            //проверка соответствия паролей
            if ($fields['pass1'] !== $fields['pass2']){
                $this->errors['pass1'] = 'Пароли не совпадают!';
                $this->errors['pass2'] = 'Пароли не совпадают!';
                $result = false;
            }

            $password = $fields['pass1'];
            //проверка разрешенной длины пароля
            if (strlen($password) < 5){
                $this->errors['pass1'] = 'Минимальная длина пароля - 5 символов';
                $this->errors['pass2'] = 'Минимальная длина пароля - 5 символов';
                $result = false;
            }

            //проверка соответствия email
            if (!preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i', $fields['email'])) {
                $this->errors['email'] = 'Указанный email некорректен.';
                $result = false;
            }

            //проверка правильности написания url
            global $urlPattern;
            if (!empty($fields['homepage']) && !preg_match(
                    $urlPattern,
                    $fields['homepage'])
            ) {
                $this->errors['homepage'] = 'URL указан неверно. Необязательное поле к заполнению.';
                $result = false;
            }

            //валидация пройдена
            return $result;
        }
        return false;
    }
}
