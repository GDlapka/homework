function setInvalid(name){
    let items = document.getElementsByName(name);
    let index;
    for (index = 0; index < items.length; ++index){
        items[index].classList.add('is-invalid');
    }
}

function setValue(name, value, error = null){
    let items = document.getElementsByName(name);
    if (items.length != 0)
        items[0].value = value;
}

function setFeedback(name, feedback){
    if (!name) return false;
    let id = name[0].toUpperCase() + name.slice(1);
    id = 'validationServer' + id + 'Feedback';
    document.getElementById(id).innerText = feedback;
}

function setValidateClasses(errors){
    for (let pair of errors.entries()){
        setInvalid(pair[0]);
        setFeedback(pair[0],pair[1]);
    }
}

function setFiels(signup){
    for (let pair of signup.entries())
        setValue(pair[0], pair[1]);
}