<?php
error_reporting(E_ALL);

use vendor\core\Router;

$query = rtrim($_SERVER['QUERY_STRING'], '/');

define('WWW', __DIR__);
define('CORE', dirname(__DIR__) . '/vendor/core');
define('ROOT', dirname(__DIR__));
define('APP', dirname(__DIR__) . '/app');
define('UPLOADS_TEMP', ROOT . '/public/uploads/temp');
define('UPLOADS_RELATIVE', '/public/uploads');
define('UPLOADS', ROOT . UPLOADS_RELATIVE);

define('LAYOUT', 'default');
define('pOffset', '25');

require '../vendor/libs/functions.php';

spl_autoload_register(function ($class) {
    $file = ROOT . '/' . str_replace('\\', '/', $class) . '.php';
    if (is_file($file)){
        require_once $file;
    }
});

Router::add('^notes/(?P<alias>[a-z-]+)$', ['controller' => 'Book', 'action' => 'notes', 'alias' => 'P']);
Router::add('^notes$', ['controller' => 'Book', 'action' => 'notes']);
Router::add('^add$', ['controller' => 'Book', 'action' => 'add']);
Router::add('^getmessage$', ['controller' => 'Book', 'action' => 'notes', 'alias' => 'getmessage']);

Router::add('^upload$', ['controller' => 'Upload']);

//default routes
Router::add('^$', ['controller' => 'Book', 'action' => 'notes', 'alias' => 'end']);
Router::add('^(?P<controller>[A-Z-]+)/?(?P<action>[a-z-]+)?$');

session_start();

Router::dispatch($query);
